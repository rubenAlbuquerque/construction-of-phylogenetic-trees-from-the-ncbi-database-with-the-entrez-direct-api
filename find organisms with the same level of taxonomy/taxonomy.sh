#!/bin/bash

#export EDIRECT_DO_AUTO_ABBREV="true"

# Install : sudo apt install ncbi-entrez-direct

echo "Inicio ..."


org=$1
rank=$2

#org="Psammodromus algirus"
#rank="genus"

#echo "$org" 
#echo "$rank"

org_with_rank=$(
esearch -db taxonomy -query "$org" | \
efetch -db taxonomy -format xml | \
xtract -pattern Taxon -block "*/Taxon" \
    -if Rank -equals "genus" \
      -element ScientificName)

#echo "$org_with_rank"
echo "-----   -------------------------------------"


#list_org=$(
esearch -db taxonomy -query "$org_with_rank[organism]" | \
efetch -db taxonomy -format docsum | \
xtract -pattern DocumentSummary \
-if Rank -equals "species" \
-element ScientificName | tr "\n" "," > OrganismsNames.txt
echo "----"

#cat OrganismsNames.txt 

#mails=$(cat OrganismsNames.txt| tr "" "\n")
#echo "------------------------"
#for addr in $mails
#do
#    echo "> [$addr]"
#done



