# Construction of Phylogenetic Trees from the NCBI Database

Goals:


*  Determine the position of a taxa in a phylogeny of a group to be determined:
*  - Choose the rate;
*  - Choose the group/rank;
*  - Get all the genes from the taxa;
*  - Filter genes by common;
*  - Alignments;
*  - Construction of phylogenetic trees;
*  - Biological analysis of the tree;




# Requirements:
# Accepted parameters:
# How to use:


